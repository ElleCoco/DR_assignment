import sys, random, time
from time import sleep
import numpy as np
import cv2
#from skimage.io import imread
import matplotlib.pyplot as plt

import naoqi
from naoqi import ALModule, ALProxy, ALBroker
#import Modules.GazeFollowing.predict_gaze as gaze
from Modules import Camera, learning
from Modules.ReactToTouch import *

# general variables
ip = "192.168.1.104"
port = 9559

#initiate proxies
try:
    tts = ALProxy("ALTextToSpeech", ip , port )
    touchProxy = ALProxy("ALTouch", ip, port)
    memory = ALProxy("ALMemory", ip, port)
    pythonBroker = ALBroker("pythonBroker","0.0.0.0", 9600, ip, port)
except Exception, e:
    print "could not create all proxies"
    print "error was ", e
    sys.exit(1)

# disable ALAutonomousMoves bug
am = ALProxy("ALAutonomousMoves", ip ,port )
am.setExpressiveListeningEnabled(False)
am.setBackgroundStrategy("none")

ReactToTouch = ReactToTouch("ReactToTouch", memory)
ReactToTouch.subscribeTouch()
wasTouched = False

start = time.time()
end  = time.time()
duration = 20

while end - start < duration:
    if ReactToTouch.parts != []:
        print 'touched bodyparts are: \n', ReactToTouch.parts
        parts = ReactToTouch.parts
        ReactToTouch.parts = []  # make the list empty again
        for part in parts:
            print 'current part is ', part
            if "Foot" in part:
                ReactToTouch.unsubscribeTouch()
                tts.say("you touched my foot")
                break
            if "Head" in part:
                ReactToTouch.unsubscribeTouch()
                tts.say("you touched my head")
                break
    end = time.time()
    ReactToTouch.subscribeTouch()

ReactToTouch.unsubscribeTouch()

