from main import *
from Modules import Camera

ip = "192.168.1.143"
port = 9559

videoProxy, cam = Camera.setupCamera(ip, port)
# try find a face
image = Camera.getFrame(videoProxy, cam)
# once found, follow the gaze
hw = image.shape[0] / 2.0
hh = image.shape[1] / 2.0
print hh

motionProxy.rest()

try:
    prediction = getGazeDirection(image)  # [x, y] coordinates of the gaze location
    print prediction
    if prediction:
        coordHeadMovement(prediction[0], prediction[1])
        time.sleep(2)
except:
    print("ERROR: getGaze failed")
finally:
    motionProxy.setStiffnesses(h_joints, 0.6)
    motionProxy.angleInterpolation(h_joints, [0, 0], 0.7, True)
    motionProxy.rest()

