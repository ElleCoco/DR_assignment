from __future__ import division

import numpy as np
import time
from skimage.io import imread
import matplotlib.pyplot as plt
import scipy.io as sio
from scipy.misc import imresize
from matplotlib.patches import Circle
import cv2
import sys
from predict_gaze import Gaze

sys.path.append('/home/theo/Software/caffe/python')
import caffe

eye_cascade = cv2.CascadeClassifier("/home/theo/Projects/DR_assignment/Assets/haarcascade_eye.xml")
face_cascade = cv2.CascadeClassifier("/home/theo/Projects/DR_assignment/Assets/haarcascade_frontalface_default.xml")
face_cascade_alt = cv2.CascadeClassifier("/home/theo/Projects/DR_assignment/Assets/haarcascade_frontalface_alt.xml")
profile_cascade = cv2.CascadeClassifier("/home/theo/Projects/DR_assignment/Assets/haarcascade_profileface.xml")


def loadModel():
    model_def = '/home/theo/Projects/DR_assignment/Modules/GazeFollowing/model/deploy_demo.prototxt'
    model_weights = '/home/theo/Projects/DR_assignment/Modules/GazeFollowing/model/binary_w.caffemodel'
    network = caffe.Net(model_def, model_weights, caffe.TEST)
    return network


def prepImages(img, e):
    """
    Output images of prepImages are exactly the same as the matlab ones

    Keyword Arguments:
    img --  image with subject for gaze calculation
    e --  head location (relative) [x, y]
    """
    input_shape = [227, 227]

    #alpha = w / x_frame
    alpha = 0.3 #0.3
    #img = imread(img)
    img_resize = None
    # height, width
    # crop of face (input 2)
    wy = int(alpha * img.shape[0])
    wx = int(alpha * img.shape[1])
    center = [int(e[0] * img.shape[1]), int(e[1] * img.shape[0])]
    y1 = int(center[1] - .5 * wy) - 1
    y2 = int(center[1] + .5 * wy) - 1
    x1 = int(center[0] - .5 * wx) - 1
    x2 = int(center[0] + .5 * wx) - 1
    # make crop of face from image
    im_face = img[y1:y2, x1:x2, :]

    # subtract mean from images
    places_mean = sio.loadmat('/home/theo/Projects/DR_assignment/Modules/GazeFollowing/model/places_mean_resize.mat')
    imagenet_mean = sio.loadmat(
        '/home/theo/Projects/DR_assignment/Modules/GazeFollowing/model/imagenet_mean_resize.mat')
    places_mean = places_mean['image_mean']
    imagenet_mean = imagenet_mean['image_mean']

    # resize image and subtract mean
    img_resize = imresize(img, input_shape, interp='bicubic')
    img_resize = img_resize.astype('float32')
    img_resize = img_resize[:, :, [2, 1, 0]] - places_mean
    img_resize = np.rot90(np.fliplr(img_resize))

    # resize eye image
    eye_image = imresize(im_face, input_shape, interp='bicubic')
    eye_image = eye_image.astype('float32')
    eye_image_resize = eye_image[:, :, [2, 1, 0]] - imagenet_mean
    eye_image_resize = np.rot90(np.fliplr(eye_image_resize))
    # get everything in the right input format for the network
    img_resize, eye_image_resize = fit_shape_of_inputs(img_resize, eye_image_resize)
    z = eyeGrid(img, [x1, x2, y1, y2])
    z = z.astype('float32')
    return img, img_resize, eye_image_resize, z


def fit_shape_of_inputs(img_resize, eye_image_resize):
    """Fits the input for the forward pass."""
    input_image_resize = img_resize.reshape([img_resize.shape[0], \
                                             img_resize.shape[1], \
                                             img_resize.shape[2], 1])
    input_image_resize = input_image_resize.transpose(3, 2, 0, 1)

    eye_image_resize = eye_image_resize.reshape([eye_image_resize.shape[0], \
                                                 eye_image_resize.shape[1], \
                                                 eye_image_resize.shape[2], 1])
    eye_image_resize = eye_image_resize.transpose(3, 2, 0, 1)
    return input_image_resize, eye_image_resize


def eyeGrid(img, headlocs):
    """Calculates the relative location of the eye.

    Keyword Arguments:
    img -- original image
    headlocs -- relative head location
    """
    w = img.shape[1]
    h = img.shape[0]
    x1_scaled = headlocs[0] / w
    x2_scaled = headlocs[1] / w
    y1_scaled = headlocs[2] / h
    y2_scaled = headlocs[3] / h
    center_x = (x1_scaled + x2_scaled) * 0.5
    center_y = (y1_scaled + y2_scaled) * 0.5
    eye_grid_x = np.floor(center_x * 12).astype('int')
    eye_grid_y = np.floor(center_y * 12).astype('int')
    eyes_grid = np.zeros([13, 13]).astype('int')
    eyes_grid[eye_grid_y, eye_grid_x] = 1
    eyes_grid_flat = eyes_grid.flatten()
    eyes_grid_flat = eyes_grid_flat.reshape(1, len(eyes_grid_flat), 1, 1)
    return eyes_grid_flat


def predictGaze(network, image, head_image, head_loc):
    """Loads data in network and does a forward pass."""
    network.blobs['data'].data[...] = image
    network.blobs['face'].data[...] = head_image
    network.blobs['eyes_grid'].data[...] = head_loc
    f_val = network.forward()
    return f_val


def postProcessing(f_val):
    """Combines the 5 outputs into one heatmap and calculates the gaze location

    Keyword arguments:
    f_val -- output of the Caffe model
    """
    fc_0_0 = f_val['fc_0_0'].T
    fc_0_1 = f_val['fc_0_1'].T
    fc_m1_0 = f_val['fc_m1_0'].T
    fc_0_1 = f_val['fc_0_1'].T
    fc_0_m1 = f_val['fc_0_m1'].T
    f_0_0 = np.reshape(fc_0_0, (5, 5))
    f_1_0 = np.reshape(fc_0_1, (5, 5))
    f_m1_0 = np.reshape(fc_m1_0, (5, 5))
    f_0_1 = np.reshape(fc_0_1, (5, 5))
    f_0_m1 = np.reshape(fc_0_m1, (5, 5))
    gaze_grid_list = [alpha_exponentiate(f_0_0), \
                      alpha_exponentiate(f_1_0), \
                      alpha_exponentiate(f_m1_0), \
                      alpha_exponentiate(f_0_1), \
                      alpha_exponentiate(f_0_m1)]
    shifted_x = [0, 1, -1, 0, 0]
    shifted_y = [0, 0, 0, -1, 1]
    count_map = np.ones([15, 15])
    average_map = np.zeros([15, 15])
    for delta_x, delta_y, gaze_grids in zip(shifted_x, shifted_y, gaze_grid_list):
        for x in range(0, 5):
            for y in range(0, 5):
                ix = shifted_mapping(x, delta_x, True)
                iy = shifted_mapping(y, delta_y, True)
                fx = shifted_mapping(x, delta_x, False)
                fy = shifted_mapping(y, delta_y, False)
                average_map[ix:fx + 1, iy:fy + 1] += gaze_grids[x, y]
                count_map[ix:fx + 1, iy:fy + 1] += 1
    average_map = average_map / count_map
    final_map = imresize(average_map, (227, 227), interp='bicubic')
    idx = np.argmax(final_map.flatten())
    [rows, cols] = ind2sub2((227, 227), idx)
    y_predict = rows / 227
    x_predict = cols / 227
    return final_map, [x_predict, y_predict]


def alpha_exponentiate(x, alpha=0.3):
    return np.exp(alpha * x) / np.sum(np.exp(alpha * x.flatten()))


def ind2sub2(array_shape, ind):
    """Python implementation of the equivalent matlab method"""
    rows = (ind / array_shape[1])
    cols = (ind % array_shape[1])  # or numpy.mod(ind.astype('int'), array_shape[1])
    return [rows, cols]


def shifted_mapping(x, delta_x, is_topleft_corner):
    if is_topleft_corner:
        if x == 0:
            return 0
        ix = 0 + 3 * x - delta_x
        return max(ix, 0)
    else:
        if x == 4:
            return 14
        ix = 3 * (x + 1) - 1 - delta_x
    return min(14, ix)


def getGaze(e, image):
    """Calculate the gaze direction in an imageself.

    Keyword arguments:
    e -- list with x,y location of head
    image -- original image
    """
    network = loadModel()
    image, image_resize, head_image, head_loc = prepImages(image, e)
    f_val = predictGaze(network, image_resize, head_image, head_loc)
    final_map, predictions = postProcessing(f_val)
    x = (1-predictions[0]) * np.shape(image)[1]
    y = (1-predictions[1]) * np.shape(image)[0]
    x = int(x)
    y = int(y)
    return [x, y]


def get_e(predictions, imgsize):
    x = predictions[0][0]
    y = predictions[0][1]
    w = predictions[0][2]
    h = predictions[0][3]

    imgw, imgh = imgsize
    return [(x + w / 2.0) / imgw, (y + h / 2.0) / imgh]


def findFace(image):
    cascades = [eye_cascade, face_cascade_alt, face_cascade, profile_cascade]
    for option in cascades:
        faces = option.detectMultiScale(image, 1.3, 5)
        if faces is not ():
            print(faces)
            return faces
    print("ERROR: face not found")
    return None


if __name__ == "__main__":
    start = time.time()
    model_def = '/home/theo/Projects/DR_assignment/Modules/GazeFollowing/model/deploy_demo.prototxt'
    model_weights = '/home/theo/Projects/DR_assignment/Modules/GazeFollowing/model/binary_w.caffemodel'
    #gazemachine = Gaze(model_def, model_weights)
    # this main method is for testing purposes
    # predictions = getGaze([0.60, 0.2679], 'script/test.jpg')
    #frame = imread('/home/theo/Projects/DR_assignment/Images/image1.jpg')
    #frame = imread('/home/theo/Projects/DR_assignment/Modules/GazeFollowing/images/5.jpg')
    #frame = imread('/home/theo/Pictures/Webcam/1.jpg')
    #f = findFace(image)
    video = cv2.VideoCapture(0)
    ret, frame = video.read()
    video.release()
    x_frame = np.shape(frame)[1]
    y_frame = np.shape(frame)[0]
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = findFace(frame)
    if faces is None:  # if failed once, flip the image
        frame = np.fliplr(frame)
        faces = findFace(frame)
        print("Image is flipped")
    if faces is not None:  # if failed twice, do not run the following code
        # e = get_e(f,(w,h))
        # Draw a rectangle around the faces
        x_face = 0
        y_face = 0
        for (x, y, w, h) in faces:
            #cv2.rectangle(frame,(x,y),(x+w,y+h),(0, 255, 0), 2)
            x_face = int((x + (x + w)) / 2)
            y_face = int((y + (y + h)) / 2)  #- int(0.13*h)
        e = [x_face / x_frame, y_face / y_frame ]
        print(e)
        #e = [0.54, 0.28]
        #alpha = w / x_frame
        alpha = 0.3
        # img = imread(img)

        img_resize = None
        # height, width
        # crop of face (input 2)
        wy = int(alpha * frame.shape[0])
        wx = int(alpha * frame.shape[1])
        center = [int(e[0] * frame.shape[1]), int(e[1] * frame.shape[0])]
        y1 = int(center[1] - .5 * wy) - 1
        y2 = int(center[1] + .5 * wy) - 1
        x1 = int(center[0] - .5 * wx) - 1
        x2 = int(center[0] + .5 * wx) - 1

        print(e)
        if e[0] != 0.0 and e[1] != 0.0:
            predictions = getGaze(e,frame)
            print(predictions)
            #predictions = [int(predictions[0] * x_frame), int(predictions[1] * y_frame)]
            cv2.circle(frame, (predictions[0], predictions[1]), 10, (0, 255, 0), 2)
            cv2.rectangle(frame,(x1,y1),(x2,y2), (0, 255, 0), 2)
            cv2.line(frame, (x_face, y_face), (predictions[0], predictions[1]), (0, 255, 0), 2)
        cv2.imshow('Video', frame)
        cv2.waitKey(300)

        #e = [0.54, 0.28]
        # When everything is done, release the capture

        cv2.destroyAllWindows()

        # image = imread('/home/theo/Pictures/Webcam/3.jpg')
        '''
        fig, ax = fig, ax = plt.subplots(1)
        ax.set_aspect('equal')
        plt.imshow(image)
        ax.add_patch(Circle((e[0] * w, e[1] * h), 10))
        ax.add_patch(Circle((predictions[0], predictions[1]), 10))
        plt.show()'''