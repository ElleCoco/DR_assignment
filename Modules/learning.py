import random
import json
import time


def resetLearningProbabilities():
    pGDs = readfile()
    pGDs.append(time.strftime("%x (%H:%M)"))
    pGDs.append(0.5)
    with open('pGD_data.json', 'wb') as dump:
        dump.write(json.dumps(pGDs))
    dump.close()


def getaction():
    r = random.uniform(0, 1)
    if (get_pGD()>=r):
        return 'GD'
    else:
        return 'RL'


def update(a,time,lr=0.8):
    pGD = get_pGD()
    if(a=='GD'):
        pGD = pGD + (1-pGD) * (lr/time)
    else:
        pRL = 1-pGD
        pRL = pRL + (1-pRL) * (lr/time)
        pGD = 1-pRL
    write_pGD(pGD)


def write_pGD(pGD):
    pGDs = readfile()
    pGDs.append(pGD)
    with open('pGD_data.json', 'wb') as dump:
        dump.write(json.dumps(pGDs))
    dump.close()


def readfile():
    try:  # check if file exists
        source = open('pGD_data.json', 'rb').read()
    except:
        pGDs = []
        pGDs.append(time.strftime("%x (%H:%M)"))
        pGDs.append(0.5)
        with open('pGD_data.json', 'wb') as dump:
            dump.write(json.dumps(pGDs))
        dump.close()
        source = open('pGD_data.json', 'rb').read()
    pGDs = json.loads(source)
    return pGDs

def get_pGD():
    pGDs = readfile()
    return pGDs[-1]

def get_pRL():
    pGDs = readfile()
    return 1-pGDs[-1]