import sys, time
import naoqi
from naoqi import ALModule, ALProxy, ALBroker
from Modules import Camera, learning
#from main import *
import cv2

# general variables
# note: port and ip are imported from main!
duration = 200
time_out = 0.2

#initiate proxies
#try:
#    postureProxy = ALProxy("ALRobotPosture", ip ,port )
#    motionProxy = ALProxy("ALMotion", ip ,port )
#    tts = ALProxy("ALTextToSpeech", ip , port )
#    memory = ALProxy("ALMemory", ip, port)
#    LED = ALProxy("ALLeds", ip, port) #TODO idea: use LEDS to show which decision was made
#    pythonBroker = ALBroker("pythonBroker","0.0.0.0", 9600, ip, port)
#except Exception, e:
#    print "could not create all proxies"
#    print "error was ", e
#    sys.exit(1)


# disable ALAutonomousMoves bug
#am = ALProxy("ALAutonomousMoves", ip ,port )
#am.setExpressiveListeningEnabled(False)
#am.setBackgroundStrategy("none")

def main():

    # tts.say("this is the test phase")
   # tts.say("we will test every method here separately")
   # tts.say("note that some methods call other methods")
   # tts.say("make sure to comment the methods you do not wish to test")
   # sleep(1)
   #
   # tested and finished random head movement!
   # motionProxy.setStiffnesses(h_joints, 0.6)
   # motionProxy.angleInterpolation(h_joints, [0, 0], 1, True)
   # tts.say("test random head movement, make three movements")
   # randomHeadMovement()
   # sleep(1)
   # randomHeadMovement()
   # sleep(1)
   # randomHeadMovement()
   # sleep(1)
   # tts.say("this were three random head movements")
   # motionProxy.setStiffnesses(h_joints, 0.6)
   # motionProxy.angleInterpolation(h_joints, [0, 0], 1, True)
   #
   # motionProxy.rest()
   #
   # tts.say("test finding an object")
   # tts.say("make sure there is an object in my field of view")
   # tts.say("I will capture an image now, and try to identify an object")
   # objectInView = findObject()
   # if objectInView:
   #     tts.say("I found an object")
   # else:
   #     tts.say("I did not find an object")
   #
   # tts.say("test centering on object")
   # t = centerOnBall() #TODO once this method is implemented, make sure to correct the coordinates and to test several
   # if t:
   #     tts.say("I am centered on the object")
   # else:
   #     tts.say("I am not centered on the object")
   #
   # tts.say("testing if there is a face in view")
   # tts.say("are you ready? i will now capture an image")
   # videoProxy, cam = Camera.setupCamera(ip, port)
   # image = Camera.getFrame(videoProxy, cam)
   # if image is not False: # make sure there is an image
   #     faceInView = findFace(image)
   #     if faceInView:
   #         tts.say("I saw a face")
   #     else:
   #         tts.say("I did not see a face")
   # else:
   #     tts.say("image capture failed, but I will continue to the next test")

   image = cv2.imread("Images/wafeltosti.jpg")
   foundBall = Camera.findBallAllColours(image)
   print foundBall

   #tts.say("test is correct ball method")
   #t = isCorrectBall()
   #print t
   #
   # motionProxy.setStiffnesses(h_joints, 0.6)
   # motionProxy.angleInterpolation(h_joints, [0, 0], 1, True)
   # tts.say("test random search for an object")
   # global correctBall
   # correctBall = False
   # t = randomSearch()
   # tts.say("I spent the printed time on the random search task")
   # print "time on random search: " + str(t)
   # motionProxy.setStiffnesses(h_joints, 0.6)
   # motionProxy.angleInterpolation(h_joints, [0, 0], 1, True)

   # tts.say("test gaze following")
   # tts.say("make sure there is a face in the field of view that looks at an object")
   # pred = gazeFollow()
   # print(pred)

   #tts.say("I am now shutting down.")
   #time.sleep(1)
   # rest
   #motionProxy.rest()
   #pythonBroker.shutdown()
   # stop threads
   sys.exit(0)


# default main is called
if __name__ == "__main__":
    main()