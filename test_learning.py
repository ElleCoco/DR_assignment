from Modules import learning as l
import random

lr = 0.8

l.resetLearningProbabilities()

for i in range(1,15):
    if (l.getaction() == 'GD'):
        l.update('GD', random.uniform(15,30), lr)
    else:
        l.update('RL', random.uniform(20,40), lr)
    print('GD: ' + str(l.get_pGD()) + ' RL: ' + str(l.get_pRL()))

