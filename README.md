﻿# Joint Attention Project - Developmental Robotics

Welcome to our Joint Attention Project, where we try to teach a robot to use gaze-following.

## Content
The directory Assets contains the following relevant files:
* haarcascade_eye.xml			- for detecting the eyes
* haarcascade_frontalface_default.xml	- for detecting the front of the face
* haarcascade_profileface.xml		- for detecting faces at slight angles

The directory Images contains some test images.

The directory Papers contains some papers used for the report.

The directory Modules contains the following relevant files:
* The directory GazeFollowing		- contains all the files and scripts to get the neural network the follow the gazes
to work.
* Camera.py				- initializes the camera.
* learning.py				- contains the implementation of the learning algorithm
* ReactToTouch				- initialization of the Class to let the NAO react to touch.

Furthermore we have the:
* main.py	- the file to run, initializes all the proxies. The functions for caregiver feedback, random head movement,
finding an object, centering on a ball and gazefollowing are implemented here and fused together in the main, but these
functions use scripts from other files.
* and a lot of test_*.py to test all the individual modules.

## How to run the program
First make sure to install all dependencies, as mentioned in the technical report.

If you have installed all dependencies and downloaded all the files, please make sure all the paths are correct in the
file main.py and in all the files in the GazeFollowing module. Preprocessing.py and test_face.py do not contain any paths,
so these do not need adjustment.

Now make sure the ip and port match the Nao robot you are working with (in main.py).

Finally, run the file main.py, and enjoy the new behaviour of your Nao robot!