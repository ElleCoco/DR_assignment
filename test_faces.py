from main import *

def centerFace(face):
    # look at the face omg
    x, y = face
    if x < -0.6:
        x = -0.6
    if x > 0.6:
        x = 0.6
    if y < -0.8:
        y = -0.8
    if y > 0.8:
        y = 0.8
    yaw = -x
    pitch = y
    motionProxy.setStiffnesses(h_joints, 0.6)
    motionProxy.angleInterpolation(h_joints, [yaw, pitch], 0.7, False)\

def get_facecenter(face,w,h):
    x = face[0]
    y = face[1]
    fw = face[2]
    fh = face[3]

    movex = x + (fw / 2.0)
    movey = y + (fh / 2.0)

    movex = movex - w * 0.5
    movey = movey - h * 0.5

    movex = movex / (w * 0.5)
    movey = movey / (h * 0.5)

    return movex, movey

try:
    coordHeadMovement(0,0,True)
    videoProxy, cam = Camera.setupCamera(ip, port)
    image = Camera.getFrame(videoProxy,cam)

    cv2.imshow("faces", image)
    cv2.waitKey(0)
    fs = findFace(image)
    print "faces: ", fs
    if fs is not None:
        for f in fs:
            print "f ", f
            cv2.rectangle(image, (f[0],f[1]), (f[0]+f[2],f[1]+f[2]), (0,255,0), 2)
            cv2.imshow("face", image)
            cv2.waitKey(0)

            w, h = (image.shape[1], image.shape[0])
            print w,h
            hw = w / 2.0
            hh = h / 2.0
            print "looking for coords"
            e = get_facecenter(f, w, h)
            print "e ", e
            # movex = (e[0] - hw)/hw
            # movey = (e[1] - hh)/hh
            # print "trying to move"
            # print "x ", movex, " y ", movey
            coordHeadMovement(e[0]*0.65, e[1]*0.65, True)
            tts.say('centered on face')
            sleep(1)

    else:
        print "no faces found"

    coordHeadMovement(0, 0, True)
    motionProxy.rest()
    #pythonBroker.shutdown()
    # stop threads
    sys.exit(0)

except:
    print "something went wrong"
    try:
        motionProxy.rest()
        pythonBroker.shutdown()
        # stop threads
        sys.exit(0)
    except:
        sys.exit(0)


