import sys, random, time
from time import sleep
import numpy as np
import cv2
from skimage.io import imread
import matplotlib.pyplot as plt

import naoqi
from naoqi import ALModule, ALProxy, ALBroker
import Modules.GazeFollowing.ourGaze as ourgaze
from Modules import Camera, learning
from Modules.ReactToTouch import *

global correctBall, ReactToTouch, feedbackTime

# general variables
ip = "192.168.1.144"
#ip = "169.254.35.27"
port = 9559
#duration = 200
time_out = 0.2
h_joints = ["HeadYaw", "HeadPitch"]
searchTime = 30
maxTime = 50 # maximum time(s) on search task
n = 1 # number of trials
lr = 1

path_to_project = "/home/theo/Projects/DR_assignment/"

# different cascade strategies
eye_cascade = cv2.CascadeClassifier(path_to_project+"Assets/haarcascade_eye.xml")
face_cascade = cv2.CascadeClassifier(path_to_project+"Assets/haarcascade_frontalface_default.xml")
face_cascade_alt = cv2.CascadeClassifier(path_to_project+"Assets/haarcascade_frontalface_alt.xml")
profile_cascade = cv2.CascadeClassifier(path_to_project+"Assets/haarcascade_profileface.xml")

#initiate proxies
try:
    postureProxy = ALProxy("ALRobotPosture", ip ,port )
    motionProxy = ALProxy("ALMotion", ip ,port )
    touchProxy = ALProxy("ALTouch", ip, port)
    tts = ALProxy("ALTextToSpeech", ip , port )
    memory = ALProxy("ALMemory", ip, port)
    pythonBroker = ALBroker("pythonBroker","0.0.0.0", 9600, ip, port)
except Exception, e:
    print "could not create all proxies"
    print "error was ", e
    sys.exit(1)

# disable ALAutonomousMoves bug
am = ALProxy("ALAutonomousMoves", ip ,port )
am.setExpressiveListeningEnabled(False)
am.setBackgroundStrategy("none")

ReactToTouch = ReactToTouch("ReactToTouch", memory)

def isCorrectBall():
    start = time.time()
    end = time.time()
    global ReactToTouch, correctBall, feedbackTime
    feedbackTime = 0
    ReactToTouch.subscribeTouch()
    tts.say("Is this the ball I was looking for?")
    wasTouched = False

    while not wasTouched and end - start < 30:
        if ReactToTouch.parts != []:
            print 'touched bodyparts are: \n', ReactToTouch.parts
            parts = ReactToTouch.parts
            ReactToTouch.parts = []  # make the list empty again
            for part in parts:
                print 'current part is ', part
                if "Foot" in part:
                    ReactToTouch.unsubscribeTouch()
                    tts.say("Alright. I will continue my search for the correct ball")
                    wasTouched = True
                    correctBall = False
                    break
                if "Head" in part:
                    print "head is in part"
                    ReactToTouch.unsubscribeTouch()
                    tts.say("I found the ball!")
                    correctBall = True
                    wasTouched = True
                    break
        end = time.time()
    # make sure not to be subscribed to touch anymore!
    if not wasTouched:
        ReactToTouch.unsubscribeTouch()

    feedbackTime = feedbackTime + (time.time() - start) #return duration

def randomHeadMovement():
    # Making one random headmovement, duration or condition can be added
    yaw = random.uniform(-0.6,0.6)
    pitch = random.uniform(-0.8,0.8)
    print yaw,pitch

    motionProxy.setStiffnesses(h_joints, 0.6)
    motionProxy.angleInterpolation(h_joints, [yaw, pitch], 0.7, True)

def coordHeadMovement(x,y, abs):
    if x < -0.8:
        x = -0.8
    if x > 0.8:
        x = 0.8
    if y < -0.6:
        y = -0.6
    if y > 0.6:
        y = 0.6
    yaw = -x
    pitch = y
    print "yaw ", yaw, " pitch ", pitch
    motionProxy.setStiffnesses(h_joints, 0.6)
    motionProxy.angleInterpolation(h_joints, [yaw, pitch], 0.7, abs)

def findObject():
    objectFound = False
    # setup camera stream
    videoProxy, cam = Camera.setupCamera(ip, port)
    image = Camera.getFrame(videoProxy, cam)

    if image is not False: # make sure there is an image
        # Check if we can find a ball
        #objectDetected = Camera.findBall(image, ballColour) #note that objectDetected contains either coordinates or False
        objectDetected = Camera.findBallAllColours(image)  # note that objectDetected contains either coordinates or False

        if objectDetected != False:
            objectFound = True
            print "object detected"
            centerOnBall(objectDetected)
            isCorrectBall()
        else:
            print "No objects detected"

    return objectFound

def get_facecenter(face,w,h):
    x = face[0]
    y = face[1]
    fw = face[2]
    fh = face[3]

    movex = x + (fw / 2.0)
    movey = y + (fh / 2.0)

    movex = movex - w * 0.5
    movey = movey - h * 0.5

    movex = movex / (w * 0.5)
    movey = movey / (h * 0.5)

    return movex, movey

def centerOnBall(ballColour):
    ballCentered = False
    videoProxy, cam = Camera.setupCamera(ip, port)
    motionProxy.setStiffnesses(h_joints, 0.6)
    while(not ballCentered):
        image = Camera.getFrame(videoProxy, cam)
        coord = Camera.findBall(image, ballColour)
        print(coord)
        if coord is False:
            break
        else:
            if coord[0] < 140:
                motionProxy.angleInterpolation(h_joints, [0.1,0], 0.2, False)
            elif coord[0] > 180:
                motionProxy.angleInterpolation(h_joints, [-0.1,0], 0.2, False)
            if coord[1] < 100:
                motionProxy.angleInterpolation(h_joints, [0,-0.1], 0.2, False)
            elif coord[1] > 140:
                motionProxy.angleInterpolation(h_joints, [0,0.1], 0.2, False)
            if coord[0] > 140 and coord[0] < 180 and coord[1] > 100 and coord[1] < 140:
                ballCentered = True
    motionProxy.setStiffnesses(h_joints, 0)

def randomSearch():
    global correctBall, feedbackTime
    correctBall = False
    feedbackTime = 0
    start = time.time()
    end = time.time()
    objectFound = False

    while not objectFound or not correctBall:
        # make random headmovement
        randomHeadMovement()
        # check if ball is in frame
        objectFound = findObject()
        end = time.time() - feedbackTime
        if end - start >= searchTime:
            return -1
        # otherwise, just continue the search
    end  = time.time()
    # feedback the time spent on the task, excluding the time spent by the caregiver on giving feedback
    return end - start - feedbackTime

def gazeFollow():
    global feedbackTime, correctBall
    correctBall = False
    feedbackTime = 0
    start = time.time()
    end = time.time()

    objectFound = False
    videoProxy, cam = Camera.setupCamera(ip, port)
    while not objectFound or not correctBall:
        end = time.time() - feedbackTime
        if end - start >= searchTime:
            return -1

        # center view
        motionProxy.setStiffnesses(h_joints, 0.6)
        motionProxy.angleInterpolation(h_joints, [0, 0], 0.7, True)

        # try find a face
        image = Camera.getFrame(videoProxy, cam)
        if not image is False:
            w, h = (image.shape[1], image.shape[0])
            # once found, follow the gaze
            try:
                fs = findFace(image)
                if fs is not None:
                    for f in fs:
                        # first center on first face by using the center coordinates
                        e = get_facecenter(f, w, h)
                        coordHeadMovement(e[0] * 0.65, e[1] * 0.65, True)
                        tts.say("I will try to follow your gaze!")
                        # now get the gazedirection of this face
                        movex,movey = getGazeDirection(image, f)  # [x, y] coordinates of the gaze location
                        coordHeadMovement(movex, movey, False)
                        # locate object within gaze
                        objectFound = findObject()
                        print("objectfound is " + str(objectFound) + " and time is: " +str((time.time() - feedbackTime) - start))
            except e:
                print "something went wrong, please try again"
                print "error: ", e



    end = time.time()
    return end - start - feedbackTime  # feedback the time spent on the task

def getGazeDirection(image,f):
    model_def = path_to_project+'Modules/GazeFollowing/model/deploy_demo.prototxt'
    model_weights = path_to_project+'Modules/GazeFollowing/model/binary_w.caffemodel'
    x_frame = np.shape(image)[1]
    y_frame = np.shape(image)[0]
    hw = x_frame / 2.0
    hh = y_frame / 2.0
    f = findFace(image)
    if f is not None:
        print("f" + str(f))
        x_face = 0.0
        y_face = 0.0
        for (x, y, w, h) in f:
            # cv2.rectangle(frame,(x,y),(x+w,y+h),(0, 255, 0), 2)
            x_face = (x + (x + w)) / 2  # - 8
            y_face = (y + (y + h)) / 2 + 8
            print((x_face,y_face))
            print((x_frame, y_frame))
        e = [float(x_face) / float(x_frame), float(y_face) / float(y_frame)]
        print(e)
        if e[0] != 0.0 and e[1] != 0.0:
            try:
                predictions = ourgaze.getGaze(e,image)

                # START OF VISUALISATION
                alpha = 0.3
                wy = int(alpha * image.shape[0])
                wx = int(alpha * image.shape[1])
                center = [int(e[0] * image.shape[1]), int(e[1] * image.shape[0])]
                y1 = int(center[1] - .5 * wy) - 1
                y2 = int(center[1] + .5 * wy) - 1
                x1 = int(center[0] - .5 * wx) - 1
                x2 = int(center[0] + .5 * wx) - 1
                cv2.circle(image, (predictions[0], predictions[1]), 10, (0, 255, 0), 2)
                cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
                cv2.line(image, (x_face, y_face), (predictions[0], predictions[1]), (0, 255, 0), 2)
                cv2.imshow('Gaze', image)
                cv2.waitKey(10)


                # END OF VISUALISATION
            except e:
                predictions= (0,0)
                print("ERROR: getGaze failed")
                print(e)

    movex = (predictions[0] - hw)/hw
    movey = (predictions[1] - hh)/hh
    return (movex,movey)

def get_e(predictions,imgsize):
    x = predictions[0]
    y = predictions[1]
    w = predictions[2]
    h = predictions[3]

    imgw, imgh = imgsize
    return [(x+w/2.0)/imgw,(y+h/2.0)/imgh]

def findFace(image):
    cascades = [eye_cascade, face_cascade_alt, face_cascade, profile_cascade]
    for option in cascades:
        faces = option.detectMultiScale(image, 1.3, 5)
        if faces is not ():
            return faces
    print("ERROR: face not found")
    return None

def main():
    global correctBall
    learning.resetLearningProbabilities()
    for i in range(0, n):
        tts.say("This is iteration number " + str(i+1))
        tts.say("get ready")
        sleep(0.5)
        start = time.time()
        end = time.time()
        correctBall = False
        feedback = None

        motionProxy.setStiffnesses(h_joints, 0.6)
        motionProxy.angleInterpolation(h_joints, [0, 0], 0.7, True)

        while end - start < maxTime:  # don't try finding the ball for too long
            if correctBall:  # Go to next iteration after the ball is found
                break

            # Only decide on new action when the feedback was used for updating
            # (note that this should contain correctBall and break)
            if feedback is None:
                action = learning.getaction()
            action = 'GD'
            if action == 'RL':
                tts.say("I will randomly look around for the object")
                try:
                    feedback = randomSearch()  # feedback is time spent
                except Exception as e:
                    feedback = -1
                    print(e)
            else:
                tts.say("I will try to follow your gaze to identify the object you are looking at")
                try:
                    feedback = gazeFollow()  # feedback is time spent
                    print("gazefolow feedback: " + str(feedback))
                except Exception as e:
                    feedback = -1
                    print(e)

            if not feedback == -1:  # If object was found
                print("feedback " + action + ":" + str(feedback))
                learning.update(action, feedback, lr)
                feedback = None #feedback is turned to None after updating, just to be sure
            else:  # If an action failed, try RandomLooking
                tts.say("I cannot seem to find the ball.")
                action = 'RL'

            end = time.time()

    # shutdown
    tts.say("I am done. Bye.")
    time.sleep(1.0)
    # rest
    motionProxy.rest()
    pythonBroker.shutdown()
    # stop threads
    sys.exit(0)

#default main is called
if __name__ == "__main__":
    main()